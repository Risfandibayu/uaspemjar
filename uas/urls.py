"""uas URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from barang.views import *
from . import views
from django.contrib.auth.views import LoginView, LogoutView
from django.conf.urls.static import static


urlpatterns = [

    path('',views.index),
    path('admin/', admin.site.urls),
    # path('barang/', barang),
    path('dashboard/', dashboard, name = 'dashboard'),


    path('barang/', barang, name='barang'),
    path('tambah_barang/', tambah_barang, name='tambah_barang'),
    path('barang/ubah/<int:id_barang>',ubah_barang, name='ubah_barang'),
    path('barang/hapus/<int:id_barang>', hapus_barang, name = 'hapus_barang'),
    
    path('masuk/', LoginView.as_view(), name = 'masuk'),
    path('keluar/',LogoutView.as_view(next_page='masuk'),name='keluar'),
    path('signup/', signup, name = 'signup'),
    

    path('kategori/', kategori, name = 'kategori'),
    path('tambah_kategori/', tambah_kategori, name='tambah_kategori'),
    path('kategori/ubah/<int:id_kategori>',ubah_kategori, name='ubah_kategori'),
    path('kategori/hapus/<int:id_kategori>', hapus_kategori, name = 'hapus_kategori'),

    path('beranda/', beranda, name = 'beranda'),
    path('katalog/', katalog, name = 'katalog'),
    path('barang/detil/<int:id_barang>',detil_barang, name='detil_barang'),

]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)
