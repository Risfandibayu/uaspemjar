from django.forms import ModelForm
from barang.models import Barang, Kategori
from django import forms

class FormBarang(ModelForm):
    class Meta:
        model = Barang
        fields = '__all__'

        widgets = {
            'nama_barang': forms.TextInput({'class': 'form-control'}),
            'harga': forms.NumberInput({'class': 'form-control'}),
            'stok': forms.NumberInput({'class': 'form-control'}),
            'kategori_id': forms.Select({'class': 'form-control'}),
        
        }

class FormKategori(ModelForm):
    class Meta:
        model = Kategori
        fields = '__all__'

        widgets = {
            'nama_ktg': forms.TextInput({'class': 'form-control'}),
        
        }

