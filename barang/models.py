from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone

# Create your models here.
class Kategori(models.Model):
    nama_ktg = models.CharField('Nama Kategori', max_length=100, null=False)
    gambar_ktg = models.ImageField(upload_to='ktg/', null=False)
    
    def __str__(self):
        return self.nama_ktg

class Barang(models.Model):
    # KATEGORI_CHOICES = (
    #     ('ck', 'Cookies'),
    #     ('br', 'Brownies'),
    #     ('hp', 'Hampers'),
    #     ('cr','Cinnamon Roll')
    # )
    nama_barang = models.CharField('Nama Barang', max_length=100, null=False)
    kategori_id = models.ForeignKey(Kategori, on_delete=models.CASCADE,null=True)
    harga = models.IntegerField('Harga', null=False)
    stok = models.IntegerField('Stok', null=False)
    gambar = models.ImageField(upload_to='gambar/', null=True)
    tanggal = models.DateTimeField(auto_now_add=True, null=True)

    def __str__(self):
        return self.nama_barang


