# Generated by Django 3.1.4 on 2020-12-20 04:39

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Barang',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nama_barang', models.CharField(max_length=100, verbose_name='Nama Barang')),
                ('kategori', models.CharField(choices=[('ck', 'Cookies'), ('br', 'Brownies'), ('hp', 'Hampers'), ('cr', 'Cinnamon Roll')], max_length=2)),
                ('harga', models.IntegerField(max_length=20, verbose_name='Harga')),
                ('stok', models.IntegerField(max_length=3, verbose_name='Stok')),
                ('tgl_input', models.DateTimeField(default=django.utils.timezone.now, verbose_name='Tanggal input')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ['-tgl_input'],
            },
        ),
    ]
