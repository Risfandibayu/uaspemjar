from django.contrib import admin
from .models import Barang
from .models import Kategori


# Register your models here.
@admin.register(Barang)
class BarangAdmin(admin.ModelAdmin):
    list_display = ['nama_barang','harga', 'stok','gambar']
    list_filter = ['nama_barang', 'harga', 'stok']
    search_fields = ['nama_barang']

@admin.register(Kategori)
class KategoriAdmin(admin.ModelAdmin):
    list_display = ['nama_ktg','gambar_ktg']
    list_filter = ['nama_ktg']
    search_fields = ['nama_ktg']