from django.shortcuts import render ,redirect
from django.http import HttpResponse
from barang.models import Barang, Kategori
from barang.forms import *
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.conf import settings
from django.contrib.auth.forms import UserCreationForm

# Create your views here.
def beranda(request):
    ktg = Kategori.objects.all()

    konteks = {
        'ktg' : ktg,
    }

    return render(request, 'home/beranda.html', konteks)

def katalog(request):
    ktg = Kategori.objects.all()
    brg = Barang.objects.all()

    konteks = {
        'ktg': ktg,
        'brg': brg,

    }

    return render(request, 'home/katalog.html', konteks)

def detil_barang(request, id_barang):
    barang = Barang.objects.get(id=id_barang)
    template = 'home/produk.html'

    konteks = {
            
            'barang': barang
    }
    return render(request, template, konteks)




@login_required(login_url=settings.LOGIN_URL)
def signup(request):
    if request.POST:
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request,"User berhasil di tambah")
            return redirect('signup')
        else:
            messages.success(request,"Terjadi kesalahan")
            return redirect('signup')
    else:
        form = UserCreationForm()
        konteks = {
            'form': form,
        }
    return render(request, 'signup.html',konteks)

@login_required(login_url=settings.LOGIN_URL)
def dashboard(request):
    barangs = Barang.objects.all()
    kategoris = Kategori.objects.all()


    total_barangs = barangs.count()
    total_kategoris = kategoris.count()
    konteks = {
        'barangs': barangs,
        'total_barangs': total_barangs,
        'kategoris': kategoris,
        'total_kategoris':total_kategoris
    }


    return render(request, 'dashboard/dashboard.html',konteks)

@login_required(login_url=settings.LOGIN_URL)
def barang(request):
    brg = Barang.objects.all()

    konteks = {
        'brg' : brg,
    }

    return render(request, 'barang/barang.html',konteks)

@login_required(login_url=settings.LOGIN_URL)
def tambah_barang(request):
    if request.POST:
        form = FormBarang(request.POST, request.FILES)
        if form.is_valid:
            form.save()
            form = FormBarang()
            pesan = "Data berhasi Di simpan"

            konteks = {
                'form': form,
                'pesan' : pesan,
            }
            
            messages.success(request, "Data berhasil di simpan ")

            return redirect('/barang')
    else:

        form = FormBarang()
        konteks = {
            'form':form,
        }

    return render(request, 'crud/tambah-barang.html', konteks)

@login_required(login_url=settings.LOGIN_URL)
def ubah_barang(request, id_barang):
    barang = Barang.objects.get(id=id_barang)
    template = 'crud/ubah-brg.html'
    
    if request.POST:
        form = FormBarang(request.POST,request.FILES, instance=barang)
        if form.is_valid():
            form.save()
            
            messages.success(request, "Data berhasil di perbarui")

            return redirect('/barang')
    else:
        form = FormBarang(instance=barang)
        konteks = {
            'form': form,
            'barang': barang
        }
    return render(request, template, konteks)

@login_required(login_url=settings.LOGIN_URL)
def hapus_barang(request, id_barang):
        barang = Barang.objects.filter(id=id_barang)
        barang.delete()

        messages.success(request, 'Berhasil menghapus data produk')
        return redirect('/barang')


@login_required(login_url=settings.LOGIN_URL)
def kategori(request):
    ktg = Kategori.objects.all()

    konteks = {
        'ktg' : ktg,
    }

    return render(request, 'ktg/ktg.html', konteks)
    
@login_required(login_url=settings.LOGIN_URL)
def hapus_kategori(request, id_kategori):
        kategori = Kategori.objects.filter(id=id_kategori)
        kategori.delete()

        messages.success(request, 'Berhasil menghapus data kategori')
        return redirect('/kategori')

@login_required(login_url=settings.LOGIN_URL)
def ubah_kategori(request, id_kategori):
    kategori = Kategori.objects.get(id=id_kategori)
    template = 'crudktg/ubah-ktg.html'
    
    if request.POST:
        form = FormKategori(request.POST,request.FILES, instance=kategori)
        if form.is_valid():
            form.save()
            
            messages.success(request, "Data berhasil di perbarui")

            return redirect('/kategori')
    else:
        form = FormKategori(instance=kategori)
        konteks = {
            'form': form,
            'kategori': kategori
        }
    return render(request, template, konteks)

@login_required(login_url=settings.LOGIN_URL)
def tambah_kategori(request):
    if request.POST:
        form = FormKategori(request.POST, request.FILES)
        if form.is_valid:
            form.save()
            form = FormKategori()
            pesan = "Data berhasi Di simpan"

            konteks = {
                'form': form,
                'pesan' : pesan,
            }
            
            messages.success(request, "Data berhasil di simpan ")

            return redirect('/kategori')
    else:

        form = FormKategori()
        konteks = {
            'form':form,
        }

    return render(request, 'crudktg/tambah-ktg.html', konteks)